# Copyright © 2015 Canonical Ltd.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Authored by: Alexandros Frantzis <alexandros.frantzis@canonical.com>

include_directories(
 ${CMAKE_SOURCE_DIR}
 ${CMAKE_BINARY_DIR}
 ${MIRSERVER_INCLUDE_DIRS}
 ${DBUS_INCLUDE_DIRS}
)

add_executable(
  lsc_test_helper_wait_for_signal
  lsc_test_helper_wait_for_signal.c
)

add_executable(
  lsc_integration_tests

  run_command.cpp
  dbus_bus.cpp
  dbus_client.cpp
  spin_wait.cpp
  lsc_display_dbus_client.cpp
  lsc_input_dbus_client.cpp
  test_dbus_event_loop.cpp
  test_lsc_display_service.cpp
  test_lsc_input_service.cpp
  test_lsc_power_button_event_sink.cpp
  test_lsc_services.cpp
  test_lsc_user_activity_event_sink.cpp
  test_external_spinner.cpp
)

target_link_libraries(
   lsc_integration_tests

   lsc
   ${GTEST_BOTH_LIBRARIES}
   ${GMOCK_LIBRARIES}
)

add_test(lsc_integration_tests ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/lsc_integration_tests)

add_dependencies(lsc_integration_tests GMock)
add_dependencies(lsc_integration_tests lsc_test_helper_wait_for_signal)

install(TARGETS lsc_integration_tests lsc_test_helper_wait_for_signal
        RUNTIME DESTINATION bin)
