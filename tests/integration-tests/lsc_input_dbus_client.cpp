/*
 * Copyright © 2015 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Andreas Pokorny <andreas.pokorny@canonical.com>
 */

#include "lsc_input_dbus_client.h"

namespace lt = lsc::test;

lt::LscInputDBusClient::LscInputDBusClient(std::string const& address)
    : lt::DBusClient{
        address,
        "com.lomiri.SystemCompositor.Input",
        "/com/lomiri/SystemCompositor/Input"}
{
}

lt::DBusAsyncReplyString lt::LscInputDBusClient::request_introspection()
{
    return invoke_with_reply<lt::DBusAsyncReplyString>(
        "org.freedesktop.DBus.Introspectable", "Introspect",
        DBUS_TYPE_INVALID);
}

lt::DBusAsyncReplyVoid lt::LscInputDBusClient::request(char const* requestName, int32_t value)
{
    return invoke_with_reply<lt::DBusAsyncReplyVoid>(
        lsc_input_interface, requestName,
        DBUS_TYPE_INT32, &value,
        DBUS_TYPE_INVALID);
}

lt::DBusAsyncReplyVoid lt::LscInputDBusClient::request_set_mouse_primary_button(int32_t button)
{
    return request("setMousePrimaryButton", button);
}

lt::DBusAsyncReplyVoid lt::LscInputDBusClient::request_set_touchpad_primary_button(int32_t button)
{
    return request("setTouchpadPrimaryButton", button);
}

lt::DBusAsyncReplyVoid lt::LscInputDBusClient::request(char const* requestName, double value)
{
    return invoke_with_reply<lt::DBusAsyncReplyVoid>(
        lsc_input_interface, requestName,
        DBUS_TYPE_DOUBLE, &value,
        DBUS_TYPE_INVALID);
}

lt::DBusAsyncReplyVoid lt::LscInputDBusClient::request_set_mouse_cursor_speed(double speed)
{
    return request("setMouseCursorSpeed", speed);
}

lt::DBusAsyncReplyVoid lt::LscInputDBusClient::request_set_mouse_scroll_speed(double speed)
{
    return request("setMouseScrollSpeed", speed);
}

lt::DBusAsyncReplyVoid lt::LscInputDBusClient::request_set_touchpad_cursor_speed(double speed)
{
    return request("setTouchpadCursorSpeed", speed);
}

lt::DBusAsyncReplyVoid lt::LscInputDBusClient::request_set_touchpad_scroll_speed(double speed)
{
    return request("setTouchpadScrollSpeed", speed);
}

lt::DBusAsyncReplyVoid lt::LscInputDBusClient::request(char const* requestName, bool value)
{
    dbus_bool_t copied = value;
    return invoke_with_reply<lt::DBusAsyncReplyVoid>(
        lsc_input_interface, requestName,
        DBUS_TYPE_BOOLEAN, &copied,
        DBUS_TYPE_INVALID);
}

lt::DBusAsyncReplyVoid lt::LscInputDBusClient::request_set_touchpad_two_finger_scroll(bool enabled)
{
    return request("setTouchpadTwoFingerScroll", enabled);
}

lt::DBusAsyncReplyVoid lt::LscInputDBusClient::request_set_touchpad_tap_to_click(bool enabled)
{
    return request("setTouchpadTapToClick", enabled);
}

lt::DBusAsyncReplyVoid lt::LscInputDBusClient::request_set_touchpad_disable_with_mouse(bool enabled)
{
    return request("setTouchpadDisableWithMouse", enabled);
}

lt::DBusAsyncReplyVoid lt::LscInputDBusClient::request_set_touchpad_disable_while_typing(bool enabled)
{
    return request("setTouchpadDisableWhileTyping", enabled);
}


